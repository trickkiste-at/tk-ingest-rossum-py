import hashlib
import hmac
import os
import logging

from flask import Flask, request, abort

app = Flask(__name__)

# Get environment variables
SECRET_KEY = os.getenv('ROSSUM_SHARED_SECRET')
logging.warning("Secret: " + SECRET_KEY)

@app.route("/", methods=["GET"])
def index():
    return "API active", 200
    

@app.route("/test_hook", methods=["POST"])
def test_hook():
    data = request.data
    headers = request.headers
    logging.warning('Headers: \n%s', headers)
    logging.warning('Body: \n%s', data)

    digest = hmac.new(SECRET_KEY.encode(), request.data, hashlib.sha1).hexdigest()
    logging.warning('Digest: \n%s', digest)
    try:
        prefix, signature = request.headers["X-Elis-Signature"].split("=")
    except ValueError:
        abort(401, "Incorrect header format")

    if not (prefix == "sha1" and hmac.compare_digest(signature, digest)):
        abort(401, "Authorization failed.")

    return "Webhook verified", 200

if __name__ == '__main__':
    app.run(debug=True)