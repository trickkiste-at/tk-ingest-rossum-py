from flask import Flask, request, abort
import hmac
import hashlib
import base64
import logging

app = Flask(name)



# Get environment variables
SECRET = os.getenv('ROSSUM_SHARED_SECRET')



def verify_webhook(data, hmac_header):
    digest = hmac.new(SECRET, data.encode('utf-8'), hashlib.sha256).digest()
    computed_hmac = base64.b64encode(digest)



return hmac.compare_digest(computed_hmac, hmac_header.encode('utf-8'))



@app.route('/webhook', methods=['POST'])
def handle_webhook():
    data = request.get_data()
    headers = flask.request.headers
    verified = verify_webhook(data, request.headers.get('X-Shopify-Hmac-SHA256'))
    logging.info("Request headers:\n" + headers)
    logging.info("Request body:\n" + data)
    logging.info("Request verified:\n" + verified)
    


if not verified:
        abort(401)




